import {
  Exercise,
  Submission,
  SubmissionResponse,
  Module,
} from "../modules/modules.types";
import { EXERCISE_STORE } from "./exercise.store";

export type Progression = {
  module?: Module;
  currentExercise?: Exercise;
  finished: number;
  total: number;
  submission?: SubmissionResponse;
};

export const PROGRESSION_INITIAL_STATE: Progression = {
  finished: 0,
  total: 0,
};

export type ProgressionAction = {
  type: EXERCISE_STORE;
  payload: Progression;
};

export const RUST_DEFAULT_EDITOR = `
fn main() {
  //Code goes here
  println!("Hello world!");
}
`;

export const PYTHON_DEFAULT_EDITOR = `
def main():
  #Code goes here
  print("Hello world!")

main()
`;

export const JAVA_DEFAULT_EDITOR = `
public class Main {
  public static void main(String[] args) {
    //Code goes here
    System.out.println("Hello World!");
  }
}
`;

export const JAVASCRIPT_DEFAULT_EDITOR = `
function main() {
  //Code goes here
  console.log("Hello World!");
}

main()
`;
