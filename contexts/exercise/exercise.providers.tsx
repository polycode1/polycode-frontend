import axios from "axios";
import {
  Dispatch,
  useContext,
  useReducer,
  createContext,
  useState,
  useEffect,
} from "react";
import { getLastExerciseSubmitted } from "../../modules/components/CardModule";
import { useHttpDispatch } from "../http/http.providers";
import { HTTP_STORE } from "../http/http.store";
import { HTTP_ACTION } from "../http/http.types";
import { Exercise, SubmissionResponse } from "../modules/modules.types";
import {
  ExerciseAction,
  EXERCISE_INITIAL_STATE,
  PostSubmission,
  Submission,
  Module,
} from "../modules/modules.types";
import { exerciseReducer } from "./exercise.reducers";
import { EXERCISE_STORE } from "./exercise.store";
import { ProgressionAction, PROGRESSION_INITIAL_STATE } from "./exercise.type";

const ExerciseContext = createContext(PROGRESSION_INITIAL_STATE);
const ExerciseDispatchContext = createContext(
  (() => 0) as React.Dispatch<ProgressionAction>
);

export const useExerciseDispatch = () => {
  return useContext(ExerciseDispatchContext);
};

export const useExercise = () => {
  return useContext(ExerciseContext);
};

export const useSubmission = () => {
  const [submission, setSubmission] = useState<null | PostSubmission>(null);
  const dispatch = useExerciseDispatch();
  const httpDispatch = useHttpDispatch();

  useEffect(() => {
    const loadSubmission = () => {
      httpDispatch({
        type: HTTP_STORE.LOADING,
      });
      axios
        .post(`${process.env.NEXT_PUBLIC_API_URL}/submissions`, submission)
        .then((res) => {
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: { status: "success", message: "Submission send" },
          });
          dispatch({
            type: EXERCISE_STORE.SET_SUBMISSION,
            payload: res.data,
          });
        })
        .catch((err) => {
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: { status: "error", message: err.response.data.message },
          });
        })
        .finally(() => {
          httpDispatch({
            type: HTTP_STORE.CLEAR_HTTP,
          });
        });
    };
    if (submission) {
      loadSubmission();
    }
  }, [submission]);
  return setSubmission;
};

export const dispatchExercise = (
  dispatch: Dispatch<ProgressionAction>,
  module: Module,
  exercise: Exercise | null
): void => {
  console.log("call dispatchExercise");
  if (module) {
    console.log("dispacthExercise", module, exercise);
    const currentExercise =
      exercise ?? module.exercises[getLastExerciseSubmitted(module)];
    let submissionExo: undefined | SubmissionResponse = undefined;
    if (currentExercise.submissions && currentExercise.submissions.length > 0) {
      submissionExo = {
        statusCode: 0,
        status: currentExercise.submissions[0].status,
        stdout: "",
        stderr: "",
      };
    }

    dispatch({
      type: EXERCISE_STORE.SET_EXERCISE,
      payload: {
        module: module,
        currentExercise: currentExercise,
        finished: getLastExerciseSubmitted(module),
        total: module.exercises.length,
        submission: submissionExo,
      },
    });
  }
};

export const ExerciseProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [exercise, dispatch] = useReducer(
    exerciseReducer,
    PROGRESSION_INITIAL_STATE
  );

  return (
    <ExerciseContext.Provider value={exercise}>
      <ExerciseDispatchContext.Provider value={dispatch}>
        {children}
      </ExerciseDispatchContext.Provider>
    </ExerciseContext.Provider>
  );
};
