import {
  Exercise,
  ExerciseAction,
  EXERCISE_INITIAL_STATE,
} from "../modules/modules.types";
import { EXERCISE_STORE } from "./exercise.store";
import { Progression } from "./exercise.type";

export const exerciseReducer = (state: Progression, action: any) => {
  switch (action.type) {
    case EXERCISE_STORE.GET_EXERCISE:
      return { ...state, ...action.payload };
    case EXERCISE_STORE.SET_EXERCISE:
      return { ...state, ...action.payload };
    case EXERCISE_STORE.SET_SUBMISSION:
      return { ...state, submission: action.payload };
    default:
      return state;
  }
};
