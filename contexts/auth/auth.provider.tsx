import { AxiosRequestConfig } from "axios";
import { useEffect, useState } from "react";
import { useUser } from "../user/user.providers";

function authHeader(accessToken: string): AxiosRequestConfig<any> {
  return {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };
}

const useAuthHeader = (): AxiosRequestConfig<any> | undefined => {
  const { user } = useUser();

  const [accessToken, setAccessToken] = useState<string | null>(null);

  useEffect(() => {
    if (user && user.access_token) {
      setAccessToken(user.access_token);
    }
  }, [user]);

  if (!accessToken) return undefined;
  else return authHeader(accessToken);
};

export default useAuthHeader;
