import { USER_STORE } from "./user.store";

export type UserSignin = {
  email: string;
  password: string;
};

export type UserObject = {
  id: number;
  username: string;
  email: string;
  verified: boolean;
  access_token: string;
};

export type UserState = {
  isLoggedIn: boolean;
  user?: UserObject;
  loading: boolean;
};

export type UserAction = {
  type: USER_STORE;
  payload?: UserObject | updateUserDto;
};

export const USER_INITIAL_STATE: UserState = {
  isLoggedIn: false,
  loading: false,
  user: undefined,
};

export type updateUserDto = {
  username: string;
};

export type updatePasswordUserDto = {
  oldPassword: string;
  newPassword: string;
};
