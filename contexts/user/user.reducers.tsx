import { USER_STORE } from "./user.store";
import { UserAction, UserObject, UserState } from "./user.types";

export const userReducer = (user: UserState, action: UserAction): UserState => {
  switch (action.type) {
    case USER_STORE.LOGIN:
      if (typeof window !== "undefined") {
        localStorage.setItem("user", JSON.stringify(action.payload));
      }
      return {
        ...user,
        isLoggedIn: true,
        user: action.payload as UserObject,
        loading: false,
      };

    case USER_STORE.UPDATE:
      const updatedUser = { ...user.user, ...action.payload };
      if (typeof window !== "undefined") {
        localStorage.setItem("user", JSON.stringify(updatedUser));
      }
      return {
        ...user,
        isLoggedIn: true,
        user: updatedUser as UserObject,
        loading: false,
      };

    case USER_STORE.LOGOUT:
      if (typeof window !== "undefined") {
        localStorage.removeItem("user");
      }
      return { ...user, isLoggedIn: false, user: undefined, loading: false };
    case USER_STORE.SET_VERIFIED:
      return { ...user, user: { ...user.user, verified: true } as UserObject };
    default:
      return user;
  }
};
