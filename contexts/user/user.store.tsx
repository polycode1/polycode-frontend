export const enum USER_STORE {
  LOGIN,
  LOGOUT,
  LOADING,
  SET_VERIFIED,
  UPDATE,
}
