import axios from "axios";
import React, {
  createContext,
  Dispatch,
  useContext,
  useEffect,
  useReducer,
  useState,
} from "react";
import useAuthHeader from "../auth/auth.provider";
import { useHttpDispatch } from "../http/http.providers";
import { HTTP_STORE } from "../http/http.store";
import { userReducer } from "./user.reducers";
import { USER_STORE } from "./user.store";
import {
  updatePasswordUserDto,
  updateUserDto,
  UserAction,
  USER_INITIAL_STATE,
} from "./user.types";

const UserContext = createContext(USER_INITIAL_STATE);
const UserDispatchContext = createContext(
  (() => 0) as React.Dispatch<UserAction>
);

export const useUserDispatch = () => {
  return useContext(UserDispatchContext);
};

export const usePatchUser = () => {
  const [user, setUser] = useState<
    updateUserDto | updatePasswordUserDto | null
  >(null);
  const authHeader = useAuthHeader();
  const dispatch = useUserDispatch();
  const { user: userLoggin } = useUser();

  const httpDispatch = useHttpDispatch();

  useEffect(() => {
    if (user && authHeader && userLoggin) {
      // authHeader.data = user;
      console.log(user);
      httpDispatch({
        type: HTTP_STORE.LOADING,
      });
      axios({
        method: "patch",
        url: `${process.env.NEXT_PUBLIC_API_URL}/users/${userLoggin.id}`,
        ...authHeader,
        data: user,
      })
        // .patch(`${process.env.NEXT_PUBLIC_API_URL}/users/${userLoggin.id}`, authHeader)
        .then((res) => {
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: { status: "success", message: "User updated" },
          });
          if ((user as updateUserDto).username) {
            dispatch({
              type: USER_STORE.UPDATE,
              payload: user as updateUserDto,
            });
          }
        })
        .catch((err) => {
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: { status: "error", message: err.response.data.message },
          });
        })
        .finally(() => {
          httpDispatch({
            type: HTTP_STORE.CLEAR_HTTP,
          });
          setUser(null);
        });
    }
  }, [user, authHeader, userLoggin]);

  return setUser;
};

export const useVerify = () => {
  const [token, setToken] = useState<string | string[]>("");
  const httpDispatch = useHttpDispatch();
  const userDispatch = useUserDispatch();

  useEffect(() => {
    const sendToken = () => {
      httpDispatch({
        type: HTTP_STORE.LOADING,
      });
      axios
        .post(`${process.env.NEXT_PUBLIC_API_URL}/users/verify/${token}`)
        .then((res) => {
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: { status: "success", message: "User verified" },
          });
          userDispatch({
            type: USER_STORE.SET_VERIFIED,
          });
        })
        .catch((err) => {
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: { status: "error", message: err.response.data.message },
          });
        })
        .finally(() => {
          httpDispatch({
            type: HTTP_STORE.CLEAR_HTTP,
          });
          setToken("");
        });
    };
    if (token) {
      sendToken();
    }
  }, [token]);

  return setToken;
};

export const useUser = () => {
  const userContext = useContext(UserContext);
  const dispatch = useUserDispatch();

  useEffect(() => {
    dispatch({ type: USER_STORE.LOADING });
    if (!userContext.isLoggedIn && typeof window !== "undefined") {
      const userStored = localStorage.getItem("user");
      if (userStored) {
        dispatch({
          type: USER_STORE.LOGIN,
          payload: JSON.parse(userStored ?? ""),
        });
      }
    }
  }, [userContext]);

  return userContext;
};

export const UserProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [user, dispatch] = useReducer(userReducer, USER_INITIAL_STATE);

  return (
    <UserContext.Provider value={user}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserContext.Provider>
  );
};
