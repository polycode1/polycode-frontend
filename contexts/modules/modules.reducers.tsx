import { MODULES_STORE } from "./modules.store";
import { Module, ModuleAction } from "./modules.types";

export const moduleReducer = (modulesState: Module[], action: ModuleAction) => {
  switch (action.type) {
    case MODULES_STORE.GET_MODULES:
      return action.payload;

    case MODULES_STORE.SET_MODULES:
      return {
        ...modulesState,
        modules: action.payload,
      };

    case MODULES_STORE.SET_LOADING:
      return {
        ...modulesState,
        loading: action.payload,
      };

    case MODULES_STORE.POST_SUBMISSION:

    default:
      return modulesState;
  }
};
