import axios from "axios";
import { config } from "process";
import {
  createContext,
  Dispatch,
  useContext,
  useEffect,
  useReducer,
  useState,
} from "react";
import useAuthHeader from "../auth/auth.provider";
import authHeader from "../auth/auth.provider";
import { useHttpDispatch } from "../http/http.providers";
import { HTTP_STORE } from "../http/http.store";
import { useUser } from "../user/user.providers";
import { moduleReducer } from "./modules.reducers";
import { MODULES_STORE } from "./modules.store";
import {
  Exercise,
  Module,
  ModuleAction,
  MODULE_INITIAL_STATE,
} from "./modules.types";
// require('dotenv').config();

const ModulesContext = createContext(MODULE_INITIAL_STATE);
const ModulesDispatchContext = createContext(
  (() => 0) as React.Dispatch<ModuleAction>
);

// export const useModules = () => {
//   return useContext(ModulesContext);
// };

export const useModulesDispatch: Dispatch<ModuleAction> = () => {
  return useContext(ModulesDispatchContext);
};

export const useModules = () => {
  const [modules, setModules] = useState<Module[] | null>([]);
  const [load, setLoad] = useState(false);
  const httpDispatch = useHttpDispatch();
  const authHeader = useAuthHeader();

  useEffect(() => {
    const loadModules = () => {
      httpDispatch({
        type: HTTP_STORE.LOADING,
      });
      axios
        .get(`${process.env.NEXT_PUBLIC_API_URL}/modules`, authHeader)
        .then((res) => {
          setModules(res.data);
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: { status: "success" },
          });
        })
        .catch((err) => {
          const errorMessage =
            err?.code === "ERR_NETWORK"
              ? "Network Error"
              : err?.response?.data?.message ?? "Unknown Error";
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: { status: "error", message: errorMessage },
          });
        })
        .finally(() => {
          httpDispatch({
            type: HTTP_STORE.CLEAR_HTTP,
          });
          setLoad(false);
        });
    };

    if (authHeader && load) loadModules();
  }, [modules, load, authHeader]);

  return { setLoad, modules };
};

export const ModulesProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [modules, dispatch] = useReducer(moduleReducer, MODULE_INITIAL_STATE);

  return (
    <ModulesContext.Provider value={modules}>
      <ModulesDispatchContext.Provider value={dispatch}>
        {children}
      </ModulesDispatchContext.Provider>
    </ModulesContext.Provider>
  );
};
