import { EXERCISE_STORE } from "../exercise/exercise.store";
import { MODULES_STORE } from "./modules.store";

export type Exercise = {
  id: number;
  name: string;
  statement: string;
  solution?: string;

  moduleId: number;
  // ide: string;
  language: string;
  submissions: Submission[];
};

export const EXERCISE_INITIAL_STATE: Exercise = {
  id: 0,
  name: "",
  statement: "",
  solution: "",
  moduleId: 0,
  language: "",
  submissions: [],
};

export type ExerciseAction = {
  type: EXERCISE_STORE;
  payload: Exercise;
};

export type Language = "rust" | "python" | "java" | "javascript";

export type SubmissionResponse = {
  statusCode: number;
  stdout: string;
  stderr: string;
  status: "started" | "finished";
};

export type PostSubmission = {
  userId: number;
  exerciseId: number;
  language: Language;
  code: string;
};

export type Submission = {
  userId: number;
  exerciseId: number;
  status: "started" | "finished"; //TODO verify here the status
  code: string;
};

export type LanguageObject = {
  id: number;
  value: Language;
};

export type Module = {
  id: number;
  name: string;
  description: string;
  type: "challenge" | "practice";
  exercises: Exercise[];
  languages: LanguageObject[];
};

export type ModuleProvider = {
  modules: Module[];
  loading: boolean;
};

export type ModuleAction = {
  type: MODULES_STORE;
  payload: Module[];
};

export const MODULE_INITIAL_STATE: Module[] = [];
