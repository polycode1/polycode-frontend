import { HTTP_STORE } from "./http.store";

export type HTTP_STATE = {
  status: "progressing" | "success" | "error" | null;
  message?: string;
  loading?: boolean;
  redirect?: string;
};

export type HTTP_ACTION = {
  type: HTTP_STORE;
  payload?: HTTP_STATE;
};

export const HTTP_INITIAL_STATE: HTTP_STATE = {
  status: null,
  message: "",
  loading: false,
  redirect: "",
};
