import { HTTP_STORE } from "./http.store";
import { HTTP_ACTION, HTTP_INITIAL_STATE, HTTP_STATE } from "./http.types";

export const httpReducer = (
  state: HTTP_STATE,
  action: HTTP_ACTION
): HTTP_STATE => {
  switch (action.type) {
    case HTTP_STORE.LOADING:
      return { ...state, loading: true, status: "progressing" };

    case HTTP_STORE.SET_HTTP:
      return { ...state, ...action.payload, loading: false };

    case HTTP_STORE.CLEAR_HTTP:
      return { ...state, loading: false };

    default:
      return state;
  }
};
