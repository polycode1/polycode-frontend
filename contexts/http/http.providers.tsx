import { createContext, Dispatch, useContext, useReducer } from "react";
import { httpReducer } from "./http.reducers";
import { HTTP_ACTION, HTTP_INITIAL_STATE } from "./http.types";

const HttpContext = createContext(HTTP_INITIAL_STATE);

const HttpDispatchContext = createContext(
  (() => 0) as React.Dispatch<HTTP_ACTION>
);

export const useHttp = () => {
  return useContext(HttpContext);
};

export const useHttpDispatch = () => {
  return useContext(HttpDispatchContext);
};

export const HttpProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [http, dispatch] = useReducer(httpReducer, HTTP_INITIAL_STATE);

  return (
    <HttpContext.Provider value={http}>
      <HttpDispatchContext.Provider value={dispatch}>
        {children}
      </HttpDispatchContext.Provider>
    </HttpContext.Provider>
  );
};
