import {
  Alert,
  Container,
  Grid,
  IconButton,
  MenuItem,
  TextField,
} from "@mui/material";
import { NextPage } from "next";
import {
  Exercise as ExerciseType,
  EXERCISE_INITIAL_STATE,
  Language,
  SubmissionResponse,
} from "../contexts/modules/modules.types";
import MuiMarkdown from "mui-markdown";
import { useModulesDispatch } from "../contexts/modules/modules.providers";
import { useHttp, useHttpDispatch } from "../contexts/http/http.providers";
import Editor, { Monaco, useMonaco } from "@monaco-editor/react";
import { useRouter } from "next/router";
import React, { forwardRef, useEffect, useMemo, useRef, useState } from "react";
import { Spinner } from "../modules/components/Spinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as monaco from "@monaco-editor/react/lib/types";
import {
  faArrowRight,
  faPlay,
  faRefresh,
} from "@fortawesome/free-solid-svg-icons";
import Button from "../modules/components/Button";
import {
  dispatchExercise,
  useExercise,
  useExerciseDispatch,
  useSubmission,
} from "../contexts/exercise/exercise.providers";
import { EXERCISE_STORE } from "../contexts/exercise/exercise.store";
import axios from "axios";
import { HTTP_STORE } from "../contexts/http/http.store";
import { useUser, useUserDispatch } from "../contexts/user/user.providers";
import { HorizontalLoader } from "../modules/components/HorizontalLoader";
import Typography from "../modules/components/Typography";
import {
  JAVASCRIPT_DEFAULT_EDITOR,
  JAVA_DEFAULT_EDITOR,
  PYTHON_DEFAULT_EDITOR,
  RUST_DEFAULT_EDITOR,
} from "../contexts/exercise/exercise.type";
import { Box, height } from "@mui/system";
import { CustomLink } from "../modules/components/CustomLink";
import { theme, themeOptions } from "../modules/theme";

export const Exercise: NextPage = () => {
  const router = useRouter();
  const httpDispatch = useHttpDispatch();

  const { loading } = useHttp();

  const editorRef = useRef(null);
  const {
    currentExercise: exercise,
    module,
    total,
    finished,
    submission,
  } = useExercise();

  const setSubmission = useSubmission();

  const [nextExerciseId, setNextExerciseId] = useState(0);
  const [switchExercise, setSwitchExercise] = useState(false);
  const dispatch = useExerciseDispatch();

  const [languages, setLanguagesExercise] = useState<Language[] | null>(null);
  const [language, setLanguage] = useState<Language>("javascript");

  const [showStdout, setShowStdout] = useState(true);

  const [codeValue, setCodeValue] = useState(JAVASCRIPT_DEFAULT_EDITOR);

  const submissionDefault = useMemo((): SubmissionResponse => {
    return { stdout: "", stderr: "", status: "started", statusCode: 0 };
  }, []);

  const [submissionState, setSubmissionState] =
    useState<SubmissionResponse>(submissionDefault);

  useEffect(() => {
    if (exercise && exercise.id) {
      setNextExerciseId(exercise.id);

      if (exercise.submissions.length > 0) {
        console.log(exercise.submissions[0].code);
        setCodeValue(exercise.submissions[0].code);
      }
    }
  }, [exercise]);

  const getEditorFromLanguage = (language: Language): string => {
    switch (language) {
      case "javascript":
        return JAVASCRIPT_DEFAULT_EDITOR;
      case "rust":
        return RUST_DEFAULT_EDITOR;
      case "java":
        return JAVA_DEFAULT_EDITOR;
      case "python":
        return PYTHON_DEFAULT_EDITOR;
      default:
        return "";
    }
  };

  const handleClickEditorReset = () => {
    const languageChange: Language = module?.languages[0].value ?? "javascript";
    setLanguage(languageChange);
    setCodeValue(getEditorFromLanguage(languageChange));
  };

  const handleChangeLanguage = (
    event:
      | React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
      | { target: { value: Language } }
  ) => {
    let languageChange = event.target.value;
    setCodeValue(getEditorFromLanguage(languageChange as Language));
    setLanguage(languageChange as Language);
  };

  useEffect(() => {
    if (submission && submission.status) {
      setSubmissionState({
        stdout: submission.stdout,
        stderr: submission.stderr,
        status: submission.status,
        statusCode: 0,
      });
      setShowStdout(submission.stdout.length > 0);
    } else {
      setSubmissionState(submissionDefault);
    }
  }, [submission, submissionDefault]);

  const handleClickSwitchOutput = (showStdout: boolean) => {
    setShowStdout(showStdout);
  };

  const handleCodeChange = (newCode: string | undefined) => {
    setCodeValue(newCode ?? "");
  };

  const handleClickSendSubmission = () => {
    if (user?.id && exercise?.id) {
      setSubmission({
        userId: user.id,
        exerciseId: exercise.id,
        language: language,
        code: codeValue,
      });
    }
  };

  useEffect(() => {
    if (module && module.languages) {
      console.log(module.languages);
      setLanguagesExercise(module.languages.map((l) => l.value));
      handleChangeLanguage({ target: { value: module.languages[0].value } });
      // setLanguage(module.languages[0].value);
    }
  }, [module]);

  const handleNextExercise = () => {
    if (module?.exercises) {
      setNextExerciseId(
        module?.exercises.findIndex((e) => e.id === nextExerciseId) + 1
      );
    }
    setSwitchExercise(true);
  };

  useEffect(() => {
    if (
      !router.query.id ||
      typeof parseInt(router?.query?.id[0]) !== "number"
    ) {
      router.back();
    } else if (
      nextExerciseId !== exercise?.id &&
      switchExercise &&
      module?.exercises
    ) {
      dispatchExercise(dispatch, module, module.exercises[nextExerciseId]);
      setSwitchExercise(false);
    }
  }, [router, module, nextExerciseId, exercise, switchExercise]);

  const { isLoggedIn, user } = useUser();
  useEffect(() => {
    if (isLoggedIn && !user?.verified && module) {
      router.push("/" + module.type ?? "practice");
    }
  }, [isLoggedIn, user, module]);

  return (
    <Container
      sx={{
        display: "flex",
        paddingX: 0,
        minWidth: "100%",
        mt: 3,
        color: "white",
        mb: 3,
      }}
    >
      <Grid container spacing={20} paddingX={10}>
        <Grid xs={6} item container sx={{ display: "initial" }}>
          <Typography variant="h2" fontFamily={"Fascinate"} mb={4}>
            {exercise?.name ?? ""}
          </Typography>

          <MuiMarkdown
            overrides={{
              code: {
                props: {
                  style: {
                    fontFamily: "Fira code",
                    color: "white",
                    backgroundColor: themeOptions.palette.editor.dark,
                    display: "inline-block",
                  },
                } as React.HTMLProps<HTMLParagraphElement>,
              },
              p: {
                props: {
                  style: {
                    fontFamily: "Fira code",
                  } as React.HTMLProps<HTMLParagraphElement>,
                },
              },
              span: {
                props: {
                  style: {
                    fontFamily: "Fira code",
                  } as React.HTMLProps<HTMLElement>,
                },
              },
            }}
          >
            {exercise?.statement ?? ""}
          </MuiMarkdown>
        </Grid>
        <Grid xs={6} container item>
          <Grid
            xs={12}
            container
            item
            sx={{
              backgroundColor: "editor.main",
              borderTopRightRadius: "15",
              borderTopLeftRadius: "15",
            }}
          >
            <Grid xs={9} container item>
              <TextField
                select
                name="Select your programming language"
                id="outlined-select-language"
                variant="standard"
                value={language}
                sx={{
                  alignSelf: "flex-end",
                  ml: 2,
                }}
                onChange={handleChangeLanguage}
              >
                {!languages && (
                  <MenuItem
                    key={"javascript"}
                    value={"javascript"}
                    color="secondary"
                    sx={{
                      py: 0.5,
                    }}
                  >
                    <Typography
                      variant="body2"
                      color="white"
                      // sx={{mt: -1}}
                    >
                      javascript
                    </Typography>
                  </MenuItem>
                )}
                {languages &&
                  languages.map((language) => (
                    <MenuItem
                      key={language}
                      value={language}
                      color="secondary"
                      sx={{
                        py: 0.5,
                      }}
                    >
                      <Typography
                        variant="body2"
                        color="white"
                        // sx={{mt: -1}}
                      >
                        {language}
                      </Typography>
                    </MenuItem>
                  ))}
              </TextField>
            </Grid>
            <Grid xs={3} item container spacing={2} flexDirection="row">
              <Grid xs={6} item>
                <Button
                  onClick={handleClickSendSubmission}
                  sx={{
                    padding: 1,
                    margin: 1,
                  }}
                >
                  <Typography variant="body2" mr={1}>
                    Run
                  </Typography>
                  <FontAwesomeIcon icon={faPlay} />
                </Button>
              </Grid>
              <Grid
                xs={6}
                item
                display="flex"
                alignItems={"center"}
                justifyContent={"center"}
              >
                <IconButton color="inherit" onClick={handleClickEditorReset}>
                  <FontAwesomeIcon icon={faRefresh} />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
          <Editor
            loading={
              <Grid
                xs={12}
                height={"65vh"}
                item
                container
                display="flex"
                justifyContent="center"
                alignItems="center"
                sx={{
                  backgroundColor: "editor.dark",
                }}
              >
                <Spinner />
              </Grid>
            }
            theme="vs-dark"
            height={"65vh"}
            language={language}
            value={codeValue}
            defaultValue={codeValue}
            onChange={handleCodeChange}
          />
          <Grid
            item
            xs={12}
            height={"20vh"}
            display="flex"
            alignContent={"flex-start"}
            flexDirection="column"
            sx={{
              backgroundColor: "editor.dark",
              borderBottomRightRadius: 15,
              borderBottomLeftRadius: 15,
              mt: -1,
              px: 4,
            }}
          >
            <Grid
              item
              xs={12}
              container
              flex="1 1 auto"
              sx={{
                maxHeight: 60,
              }}
            >
              <Button
                name="Show the stdout"
                onClick={() => handleClickSwitchOutput(true)}
                disabled={loading}
                sx={{
                  backgroundColor: "editor.dark",
                  "&:hover": {
                    backgroundColor: "editor.main",
                  },
                  height: 50,
                }}
              >
                <Typography
                  variant="body2"
                  color="white"
                  fontFamily={"Fira Code"}
                  fontWeight={showStdout ? "900" : "500"}
                  sx={{
                    textDecoration: showStdout ? "underline" : "none",
                  }}
                >
                  Output
                </Typography>
              </Button>
              <Button
                name="Show the stderr"
                disabled={loading}
                onClick={() => handleClickSwitchOutput(false)}
                sx={{
                  height: 50,
                  backgroundColor: "editor.dark",
                  "&:hover": {
                    backgroundColor: "editor.main",
                  },
                  textDecoration: !showStdout ? "underline" : "none",
                }}
              >
                Errors
              </Button>
            </Grid>
            <Grid
              item
              xs={12}
              container
              flex="1 1 auto"
              sx={{ overflow: "scroll" }}
            >
              <Typography variant="body1" fontFamily={"Fira Code"}>
                {"> "}
                {!submissionState.stdout &&
                  !loading &&
                  !submissionState.stderr &&
                  "Write and execute some code to see something here 👁"}
                {loading && "Sending code ..."}
                {!loading && showStdout && submissionState.stdout}
                {!loading && !showStdout && submissionState.stderr}
              </Typography>
            </Grid>
            <Grid item xs={5}>
              {(submissionState.stdout || submissionState.stderr) &&
                submissionState.status === "started" && (
                  <Alert variant="filled" severity="error">
                    This is not the correct answer...
                  </Alert>
                )}
              {submissionState.status && submissionState.status === "finished" && (
                <Alert
                  variant="filled"
                  severity="success"
                  sx={{ height: "50px" }}
                >
                  Yeah you made it !
                  <CustomLink href={`/exercise?id=${nextExerciseId}`}>
                    <FontAwesomeIcon
                      icon={faArrowRight}
                      onClick={handleNextExercise}
                    />
                  </CustomLink>
                </Alert>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Exercise;
