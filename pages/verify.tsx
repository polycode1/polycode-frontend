import { useTheme } from "@emotion/react";
import { Container, Fade, Zoom } from "@mui/material";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useHttp, useHttpDispatch } from "../contexts/http/http.providers";
import { HTTP_STORE } from "../contexts/http/http.store";
import { useVerify } from "../contexts/user/user.providers";
import { CustomLink } from "../modules/components/CustomLink";
import SigninButton from "../modules/components/SingninButton";
import Typography from "../modules/components/Typography";
import ErrorLayout from "../modules/views/ErrorLayout";

const Verify: NextPage = () => {
  const router = useRouter();
  const { token } = router.query;
  const { status, loading } = useHttp();
  const [displayMessage, setDisplayMessage] = useState(false);
  const setToken = useVerify();

  useEffect(() => {
    // Make sure the token is valid
    if (token) {
      setToken(token);
    }
  }, [token]);

  useEffect(() => {
    if (status && status.length > 0) {
      setDisplayMessage(true);
    }
  }, [status]);

  const theme = useTheme();

  return (
    <>
      {(loading || !displayMessage) && (
        <Container sx={{ minHeight: "80vh" }}></Container>
      )}
      {displayMessage && !loading && !token && (
        <ErrorLayout message="The following link is invalid, a token should be provided.">
          <CustomLink href="/" underline={false} color="white">
            Teleport to a safer place 🛸
          </CustomLink>
        </ErrorLayout>
      )}
      {!loading && token && status === "error" && (
        <ErrorLayout message="The following link is invalid, token's value isn't correct">
          <CustomLink href="/" underline={false} color="white">
            Teleport to a safer place 🛸
          </CustomLink>
        </ErrorLayout>
      )}
      {!loading && status === "success" && (
        <Container
          sx={{
            mt: { xs: 5, md: 10 },
            width: "80%",
            flexDirection: "column",
            display: "flex",
            alignItems: "center",

            color: "white",
          }}
        >
          <Zoom in={true}>
            <span>
              <Typography variant="h2" marked="center" fontFamily={"Fascinate"}>
                🥳 Congrats
              </Typography>
            </span>
          </Zoom>
          <Fade in={true} timeout={1000}>
            <span>
              <Typography
                variant="h3"
                sx={{
                  mt: 10,
                }}
              >
                Your account is{" "}
                <Typography
                  variant="h3"
                  component={"span"}
                  color={"secondary.main"}
                >
                  activated
                </Typography>{" "}
                !
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  mt: 2,
                  mb: 6,
                }}
              >
                <Typography variant="h6" component={"span"} color={"info.main"}>
                  Sign in
                </Typography>{" "}
                to start coding.
              </Typography>
            </span>
          </Fade>

          <Fade in={true} timeout={1500}>
            <span>
              <SigninButton />
            </span>
          </Fade>
        </Container>
      )}
    </>
  );
};

export default Verify;
