import React, { useEffect, useState } from "react";
import Avatar from "@mui/material/Avatar";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { CustomLink } from "../modules/components/CustomLink";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { ThemeProvider } from "@mui/material/styles";
import { NextPage } from "next";
import { theme } from "../modules/theme";
import Button from "../modules/components/Button";
import { isAlphanumeric, isEmail } from "../modules/validators/validators";
import { useUserDispatch } from "../contexts/user/user.providers";
import { Alert, AlertColor, Fade, LinearProgress } from "@mui/material";
import { USER_STORE } from "../contexts/user/user.store";
import { useHttpDispatch } from "../contexts/http/http.providers";
import { HTTP_STORE } from "../contexts/http/http.store";
import { useRouter } from "next/router";
import axios from "axios";

const SignIn: NextPage = () => {
  const [data, setData] = useState({
    email: "",
    isValidEmail: false,
    password: "",
    isValidPassword: false,
  });

  const [isVerified, setVerified] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const dispatch = useUserDispatch();
  const dispatchHttp = useHttpDispatch();

  useEffect(() => {
    setVerified(data.isValidEmail && data.isValidPassword);
  }, [data]);

  const [alert, setAlert] = useState({
    type: "",
    message: "",
  });
  const router = useRouter();

  useEffect(() => {
    // Prefetch the dashboard page
    router.prefetch("/practice");
  }, []);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    setLoading(true);
    dispatchHttp({
      type: HTTP_STORE.LOADING,
    });
    axios
      .post(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
        email: data.get("email"),
        password: data.get("password"),
      })
      .then((response) => {
        dispatch({
          type: USER_STORE.LOGIN,
          payload: response.data,
        });
        dispatchHttp({
          type: HTTP_STORE.SET_HTTP,
          payload: {
            status: "success",
            message: "Login Successful",
            redirect: "/practice",
          },
        });
      })
      .catch((error) => {
        setAlert({
          type: "error",
          message: "Wrong email or password",
        });
        dispatchHttp({
          type: HTTP_STORE.SET_HTTP,
          payload: { status: "error", message: "Wrong email or password" },
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleChangeEmail = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const email = event.target.value;
    const isValidEmail = isEmail(email);
    setData({ ...data, email: email, isValidEmail: isValidEmail });
  };

  const handleChangePassword = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const password = event.target.value;
    //TODO Check if password is strong (contains at least one number, one uppercase letter, one lowercase letter, and one special character)
    const isValidPassword = password.length > 7;
    setData({ ...data, password: password, isValidPassword: isValidPassword });
  };

  return (
    <>
      {isLoading && <LinearProgress color="info" />}
      <Fade in={true} timeout={800}>
        <Container
          component="main"
          maxWidth="sm"
          sx={{
            bgcolor: "#ffffff",
            mt: 4,
            padding: 5,
            borderRadius: 5,
          }}
        >
          <Box
            sx={{
              // marginTop: 8,
              marginX: { xs: 0, sm: 8 },
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar
              sx={{
                width: 172,
                height: 172,
                m: 1,
                bgcolor: "secondary.main",
              }}
            >
              <img src="polycode_bunny_logo.png" width={128} />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign In
            </Typography>
            <Box
              component="form"
              noValidate
              onSubmit={handleSubmit}
              sx={{ mt: 3 }}
            >
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    onChange={(event) => handleChangeEmail(event)}
                    value={data.email}
                    error={data.email.length > 0 && !data.isValidEmail}
                    helperText={
                      data.email.length > 0 && !data.isValidEmail
                        ? "Please enter a valid email"
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="new-password"
                    value={data.password}
                    onChange={(event) => handleChangePassword(event)}
                    error={data.password.length > 0 && !data.isValidPassword}
                    helperText={
                      data.password.length > 0 && !data.isValidPassword
                        ? "Password must contains at least 8 characters"
                        : ""
                    }
                  />
                </Grid>
              </Grid>
              {alert.type && alert.type !== "progressing" && (
                <Alert severity={alert.type as AlertColor}>
                  {alert.message}
                </Alert>
              )}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                color="secondary"
                disabled={!isVerified || isLoading}
              >
                {isLoading ? "Signin in..." : "Sign In"}
              </Button>
              <Grid container justifyContent="flex-end">
                <Grid item>
                  <CustomLink href="/signup">
                    {"Don't have an account? Sign up"}
                  </CustomLink>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Container>
      </Fade>
    </>
  );
};

export default SignIn;
