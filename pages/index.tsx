import styled from "@emotion/styled";
import { Container, Grow, Zoom } from "@mui/material";
import { Box } from "@mui/system";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useUser } from "../contexts/user/user.providers";
import Button from "../modules/components/Button";
import TransitionModal from "../modules/components/TransitionModal";
import Typography from "../modules/components/Typography";

export const ProductHeroLayoutRoot = styled(Box)(({ theme }) => ({
  color: "white",
  display: "flex",
  alignItems: "center",
  height: "100%",
  flexDirection: "column",
  overflowWrap: "break-word",
}));

const Background = styled(Box)({
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
  zIndex: -2,
});

export const Home: NextPage = () => {
  const { isLoggedIn } = useUser();

  const router = useRouter();

  useEffect(() => {
    if (isLoggedIn) {
      router.push("/practice");
    }
  }, [isLoggedIn]);

  return (
    <>
      {/* <TransitionModal /> */}
      <Zoom in={true}>
        <ProductHeroLayoutRoot
          sx={{
            mt: { xs: "10vw", lg: 20 },
            mb: 14,
          }}
        >
          <Typography
            fontFamily={"Fascinate"}
            color="inherit"
            align="center"
            variant="h2"
            marked="center"
          >
            PolyCode
          </Typography>
          <Typography
            color="inherit"
            align="center"
            variant="h5"
            sx={{ mt: { sx: 4, sm: 6 } }}
          >
            Le site pour apprendre les codes*
          </Typography>
          <Typography
            color="inherit"
            align="center"
            variant="subtitle2"
            sx={{
              mb: 4,
              fontStyle: "italic",
            }}
          >
            *The website where to learn programming
          </Typography>
          <Button
            color="secondary"
            variant="contained"
            size="large"
            link="/signup"
            sx={{ minWidth: 0 }}
          >
            Start
          </Button>
        </ProductHeroLayoutRoot>
      </Zoom>
      <Background
        sx={{
          backgroundImage: `url("homepage_big_fond.png")`,
          backgroundColor: "#96217f", // Average color of the background image.
          backgroundPosition: "center",
          mt: { xs: -50, sm: -30, md: "0" },
          pt: { xs: "-50%", md: "0" },
          backgroundSize: { xs: "160vw", md: "130vh" },
        }}
      />
    </>
  );
};

export default Home;
