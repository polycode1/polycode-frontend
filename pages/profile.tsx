import { NextPage } from "next";
import React, { useEffect, useState } from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import { CustomLink } from "../modules/components/CustomLink";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "../modules/theme";
import Button from "../modules/components/Button";
import { isAlphanumeric, isEmail } from "../modules/validators/validators";
import axios from "axios";
import {
  usePatchUser,
  useUser,
  useUserDispatch,
} from "../contexts/user/user.providers";
import { Alert, AlertColor, LinearProgress } from "@mui/material";
import { USER_STORE } from "../contexts/user/user.store";
import { useHttp, useHttpDispatch } from "../contexts/http/http.providers";
import { HTTP_STORE } from "../contexts/http/http.store";
import { useRouter } from "next/router";
import UserAvatar from "../modules/components/UserAvatar";
import WarningAvatar from "../modules/components/WarningAvatar";

const Profile: NextPage = () => {
  const { user } = useUser();
  const [isVerified, setVerified] = useState(false);
  const { loading, status, message } = useHttp();
  const setUser = usePatchUser();
  const [data, setData] = useState({
    email: "",
    username: "",
    isValidUsername: false,
    oldPassword: "",
    isValidOldPassword: false,
    password: "",
    isValidPassword: false,
    passwordConfirmation: "",
    isValidPasswordConfirmation: false,
  });

  useEffect(() => {
    if (user) {
      setData({
        ...data,
        email: user.email,
        username: user.username,
        isValidUsername: true,
      });
    }
  }, [user]);

  const handleChangeUsername = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const username = event.target.value;
    const isValidUsername = username.length > 2 && isAlphanumeric(username);
    setData({ ...data, username: username, isValidUsername: isValidUsername });
  };

  const handleChangeOldPassword = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const password = event.target.value;
    //TODO Check if password is strong (contains at least one number, one uppercase letter, one lowercase letter, and one special character)
    const isValidPassword = password.length > 7;
    setData({
      ...data,
      oldPassword: password,
      isValidOldPassword: isValidPassword,
    });
  };

  const handleChangePassword = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const password = event.target.value;
    //TODO Check if password is strong (contains at least one number, one uppercase letter, one lowercase letter, and one special character)
    const isValidPassword =
      password.length > 7 && password !== data.oldPassword;
    const isValidPasswordConfirmation = password === data.passwordConfirmation;
    setData({
      ...data,
      password: password,
      isValidPassword: isValidPassword,
      isValidPasswordConfirmation: isValidPasswordConfirmation,
    });
  };

  const handleChangePasswordConfirmation = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    event.preventDefault();
    const passwordConfirmation = event.target.value;
    const isValidPasswordConfirmation = passwordConfirmation === data.password;
    setData({
      ...data,
      passwordConfirmation: passwordConfirmation,
      isValidPasswordConfirmation: isValidPasswordConfirmation,
    });
  };

  const handleSubmitUsername = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setUser({ username: data.username });
  };

  const handleSubmitPassword = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setUser({ oldPassword: data.oldPassword, newPassword: data.password });
  };

  return (
    <>
      <Container
        component="main"
        maxWidth="sm"
        sx={{
          bgcolor: "#ffffff",
          mt: 4,
          padding: 5,
          borderRadius: 5,
        }}
      >
        <Box
          sx={{
            // marginTop: 8,
            marginX: { xs: 0, sm: 8 },
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <UserAvatar />
          <Typography component="h1" variant="h5" fontFamily={"Fascinate"}>
            Profile
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmitUsername}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  variant="filled"
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  value={data.email}
                  disabled
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  autoComplete="given-name"
                  name="username"
                  required
                  fullWidth
                  id="username"
                  label="Username"
                  autoFocus
                  onChange={(event) => handleChangeUsername(event)}
                  value={data.username}
                  error={data.username.length > 0 && !data.isValidUsername}
                  helperText={
                    data.username.length > 0 && !data.isValidUsername
                      ? "Username must be at least 3 characters long and contain only alphanumeric characters"
                      : ""
                  }
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              disabled={!data.isValidUsername || loading}
            >
              {loading ? "Updating..." : "Update"}
            </Button>
          </Box>
        </Box>
      </Container>

      <Container
        component="main"
        maxWidth="sm"
        sx={{
          bgcolor: "#ffffff",
          mt: 8,
          mb: 8,
          padding: 5,
          borderRadius: 5,
        }}
      >
        <Box
          sx={{
            // marginTop: 8,
            marginX: { xs: 0, sm: 8 },
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <WarningAvatar />
          <Typography
            component="h1"
            variant="h5"
            fontFamily={"Fascinate"}
            sx={{ color: theme.palette.error.main }}
          >
            Danger zone
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmitPassword}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="old password"
                  label="Old password"
                  type="password"
                  id="old-password"
                  autoComplete="new-password"
                  value={data.oldPassword}
                  onChange={(event) => handleChangeOldPassword(event)}
                  error={
                    data.oldPassword.length > 0 && !data.isValidOldPassword
                  }
                  helperText={
                    data.oldPassword.length > 0 && !data.isValidOldPassword
                      ? "Password must contains at least 8 characters"
                      : ""
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="New password"
                  label="New password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                  value={data.password}
                  onChange={(event) => handleChangePassword(event)}
                  error={data.password.length > 0 && !data.isValidPassword}
                  helperText={
                    data.password.length > 0 && !data.isValidPassword
                      ? "Password must be at least 8 characters long and must not be the same as the old password"
                      : ""
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password confirmation"
                  label="Password confirmation"
                  type="password"
                  id="passwordConfirmation"
                  autoComplete="new-password"
                  value={data.passwordConfirmation}
                  onChange={(event) => handleChangePasswordConfirmation(event)}
                  error={
                    data.passwordConfirmation.length > 0 &&
                    !data.isValidPasswordConfirmation
                  }
                  helperText={
                    data.passwordConfirmation.length > 0 &&
                    !data.isValidPasswordConfirmation
                      ? "Passwords does not match"
                      : ""
                  }
                />
              </Grid>
            </Grid>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              disabled={
                !data.isValidPassword ||
                !data.isValidOldPassword ||
                !data.isValidPasswordConfirmation ||
                loading
              }
            >
              {loading ? "Updating..." : "Update password"}
            </Button>
          </Box>
        </Box>
      </Container>
    </>
  );
};

export default Profile;
