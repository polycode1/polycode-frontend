import { useTheme } from "@emotion/react";
import { faMailBulk } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Container, Fade, Grid, Zoom } from "@mui/material";
import { NextPage } from "next";
import Typography from "../modules/components/Typography";

const Welcome: NextPage = () => {
  const theme = useTheme();

  return (
    <Container
      sx={{
        mt: { xs: 5, md: 10 },
        width: "80%",
        flexDirection: "column",
        display: "flex",
        alignItems: "center",

        color: "white",
      }}
    >
      <Zoom in={true}>
        <span>
          <Typography variant="h2" marked="center" fontFamily={"Fascinate"}>
            ☺️ Welcome Abroad
          </Typography>
        </span>
      </Zoom>
      <Fade in={true} timeout={1000}>
        <span>
          <Typography
            variant="h3"
            sx={{
              mt: 10,
            }}
          >
            Thanks joining{" "}
            <Typography variant="h3" component={"span"} color="secondary.main">
              PolyCode
            </Typography>{" "}
            !
          </Typography>
          <Typography
            variant="h6"
            sx={{
              mt: 2,
              mb: 6,
            }}
          >
            To complete your registration, please verify your{" "}
            <Typography variant="h6" component={"span"} color="info.main">
              email address
            </Typography>
            .
          </Typography>
        </span>
      </Fade>

      <FontAwesomeIcon fade icon={faMailBulk} size="3x" />
    </Container>
  );
};

export default Welcome;
