import { faInfo, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Container, Grid } from "@mui/material";
import { Box } from "@mui/system";
import { NextPage } from "next";
import { useEffect } from "react";
import { useUser } from "../contexts/user/user.providers";
import Button from "../modules/components/Button";
import { CardModule } from "../modules/components/CardModule";
import SigninButton from "../modules/components/SingninButton";
import Typography from "../modules/components/Typography";
import ErrorLayout from "../modules/views/ErrorLayout";
import Modules from "../modules/views/Modules";

const Practice: NextPage = () => {
  //getModules('challenge');

  const { user } = { user: { id: 1 } }; //useUser();
  return (
    <>
      {user ? (
        <Modules type="practice" userId={user.id} />
      ) : (
        <ErrorLayout message="User not connected. 👇 Please sign in to access Practice">
          <SigninButton />
        </ErrorLayout>
      )}
    </>
  );
};

export default Practice;
