import { AppBar, Toolbar } from "@mui/material";
import { Box } from "@mui/system";
import { NextPage } from "next";
import Footer from "../modules/components/Footer";

import { Header } from "../modules/components/Header";

export const Dashboard: NextPage = () => {
  return (
    <>
      <Header />
      <Footer />
    </>
  );
};
export default Dashboard;
