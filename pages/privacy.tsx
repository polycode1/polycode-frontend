import { Box, Container, Grid } from "@mui/material";
import { ThemeProvider } from "@mui/styles";
import { ProductHeroLayoutRoot } from ".";
import Typography from "../modules/components/Typography";
import { themeLogo } from "../modules/theme";

export const Privacy: React.FC = () => {
  return (
    <ProductHeroLayoutRoot
      sx={{
        width: "50%",
        alignSelf: "center",
      }}
    >
      <Typography
        color="inherit"
        marked="center"
        variant="h2"
        mt="20"
        fontFamily={"Fascinate"}
        sx={{
          mt: 10,
          mb: 5,
        }}
      >
        Privacy policy
      </Typography>
      <ThemeProvider theme={themeLogo}>
        <Typography variant="h6">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac
          aliquet ex, ac faucibus felis. In in faucibus urna. Integer in
          faucibus diam. Integer tempor dictum purus eget dignissim. Nunc mi
          felis, sollicitudin sit amet massa at, porttitor facilisis mauris.
          Integer mattis elementum purus et aliquet. Cras scelerisque rhoncus
          odio, vitae iaculis sapien vestibulum in. Nam vitae massa a lorem
          faucibus faucibus. Aenean erat eros, lacinia nec iaculis in, blandit
          sed nunc. Etiam aliquet, mauris eu lacinia ornare, arcu nunc rutrum
          felis, quis porta sem nisi eget diam. Morbi in pharetra sapien.
          Vestibulum auctor ipsum leo, ut condimentum velit mollis sit amet.
          Integer iaculis lacus vitae nisi dapibus lacinia. Interdum et
          malesuada fames ac ante ipsum primis in faucibus. Pellentesque ut
          fermentum ex, in congue mauris. Suspendisse potenti. Aliquam erat
          volutpat. Nunc convallis sapien libero, nec luctus dui viverra ut.
          Vestibulum vel dapibus nisl, et finibus dolor. Pellentesque viverra
          facilisis porttitor. In semper felis non elementum venenatis. Maecenas
          vestibulum ac dui tempus convallis. Nam eleifend orci eget tempor
          aliquam. Nulla lacinia efficitur tempus. In ultricies augue ac
          suscipit condimentum. Phasellus quis nunc dignissim, euismod magna
          vitae, pulvinar magna. Ut tellus lacus, vehicula vitae quam ac, tempor
          convallis nisl. Quisque vitae erat eu tortor pharetra pretium ut sed
          ex. Fusce vehicula erat imperdiet nibh interdum, nec tempor orci
          mollis. In nisi sapien, rhoncus id neque eu, ullamcorper efficitur
          ante. Pellentesque eu feugiat magna. Cras ipsum tellus, lacinia id
          nibh vitae, blandit venenatis tellus. Quisque hendrerit sem in felis
          convallis porttitor. Proin pellentesque mauris id cursus aliquam.
          Vestibulum id scelerisque ex. Aliquam vulputate sollicitudin
          fermentum. Nam vulputate leo non nisl ultricies feugiat. Maecenas
          pellentesque massa a tortor sollicitudin, et cursus dui egestas.
          Vivamus pretium, massa eget hendrerit semper, ex urna eleifend ex,
          lacinia imperdiet neque arcu quis nisi. Nam quis eros urna. Mauris
          auctor leo non lorem auctor semper. Ut mattis eu enim pharetra
          sodales. Vivamus tincidunt sagittis ultrices. Vivamus mollis volutpat
          libero. Integer sit amet ultrices nulla. Donec porta felis turpis, eu
          placerat leo congue in. Aliquam venenatis sapien pulvinar accumsan
          semper. Praesent vel venenatis est. Vivamus finibus ultrices ligula,
          eu dapibus enim tempor sed. Nam feugiat vestibulum nulla eget
          vestibulum. Donec eget posuere augue, id maximus mauris. Sed vehicula
          urna sem, sed facilisis lectus facilisis ut. Ut sollicitudin ligula at
          dolor commodo venenatis. Nullam pharetra faucibus urna id scelerisque.
          Vivamus ac arcu varius, gravida dolor eget, ullamcorper magna. Sed ac
          diam ac orci placerat ullamcorper. Fusce consectetur, mi id fermentum
          blandit, lectus leo laoreet ligula, at rutrum turpis augue vel mi.
        </Typography>
      </ThemeProvider>
      {/* </Grid> */}
      {/* </Container> */}
    </ProductHeroLayoutRoot>
  );
};

export default Privacy;
