import React, { useEffect, useState, FormEvent, useReducer } from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { NextPage } from "next";
import Button from "../modules/components/Button";
import { isAlphanumeric, isEmail } from "../modules/validators/validators";
import { Alert, AlertColor, Fade, LinearProgress, Slide } from "@mui/material";
import axios from "axios";
import { CustomLink } from "../modules/components/CustomLink";
import { useHttp, useHttpDispatch } from "../contexts/http/http.providers";
import { HTTP_STORE } from "../contexts/http/http.store";

const SignUp: NextPage = () => {
  const [data, setData] = useState({
    username: "",
    isValidUsername: false,
    email: "",
    isValidEmail: false,
    password: "",
    isValidPassword: false,
    passwordConfirmation: "",
    isValidPasswordConfirmation: false,
    isValidPrivacy: false,
    isValidAge: false,
  });

  const { loading } = useHttp();
  const httpDispatch = useHttpDispatch();

  const [isVerified, setVerified] = useState(false);
  const [warning, setWarning] = useState("");
  const [alert, setAlert] = useState({
    type: "",
    message: "",
  });

  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!isVerified) {
      setWarning("Please fill all the fields correctly");
    } else {
      const data = new FormData(event.currentTarget);
      httpDispatch({
        type: HTTP_STORE.LOADING,
      });
      axios
        .post(`${process.env.NEXT_PUBLIC_API_URL}/users`, {
          username: data.get("username") as string,
          email: data.get("email") as string,
          password: data.get("password") as string,
        })
        .then(() => {
          setAlert({
            type: "success",
            message: "You have successfully signed up!",
          });
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: {
              status: "success",
              message: "You have successfully signed up!",
              redirect: "/welcome",
            },
          });
        })
        .catch((err) => {
          console.log(err);
          setAlert({
            type: "error",
            message: err.response.data.message,
          });
          httpDispatch({
            type: HTTP_STORE.SET_HTTP,
            payload: {
              status: "error",
              message: "Email address already exists",
            },
          });
        })
        .finally(() => {
          httpDispatch({
            type: HTTP_STORE.CLEAR_HTTP,
          });
        });
    }
  };

  useEffect(() => {
    setVerified(
      data.isValidUsername &&
        data.isValidEmail &&
        data.isValidPassword &&
        data.isValidPasswordConfirmation &&
        data.isValidPrivacy &&
        data.isValidAge
    );
  }, [data]);

  const handleChangeUsername = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const username = event.target.value;
    const isValidUsername = username.length > 2 && isAlphanumeric(username);
    setData({ ...data, username: username, isValidUsername: isValidUsername });
  };

  const handleChangeEmail = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const email = event.target.value;
    const isValidEmail = isEmail(email);
    setData({ ...data, email: email, isValidEmail: isValidEmail });
  };

  const handleChangePassword = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const password = event.target.value;
    //TODO Check if password is strong (contains at least one number, one uppercase letter, one lowercase letter, and one special character)
    const isValidPassword = password.length > 7;
    const isValidPasswordConfirmation = password === data.passwordConfirmation;
    setData({
      ...data,
      password: password,
      isValidPassword: isValidPassword,
      isValidPasswordConfirmation: isValidPasswordConfirmation,
    });
  };

  const handleChangePasswordConfirmation = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    event.preventDefault();
    const passwordConfirmation = event.target.value;
    const isValidPasswordConfirmation = passwordConfirmation === data.password;
    setData({
      ...data,
      passwordConfirmation: passwordConfirmation,
      isValidPasswordConfirmation: isValidPasswordConfirmation,
    });
  };

  const handleChangePrivacy = () => {
    setData({ ...data, isValidPrivacy: !data.isValidPrivacy });
  };
  const handleChangeAge = () => {
    setData({ ...data, isValidAge: !data.isValidAge });
  };

  const containerRef = React.useRef(null);

  return (
    <>
      {loading && <LinearProgress color="info" />}
      <Fade in={true} timeout={800}>
        <Container
          component="main"
          maxWidth="sm"
          sx={{ bgcolor: "#ffffff", marginY: 20, padding: 5, borderRadius: 5 }}
        >
          <Box
            ref={containerRef}
            sx={{
              // marginTop: 8,
              marginX: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar
              sx={{
                width: 172,
                height: 172,
                m: 1,
                bgcolor: "secondary.main",
              }}
            >
              <img src="polycode_bunny_logo.png" width={128} />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign up
            </Typography>
            <Box
              component="form"
              noValidate
              onSubmit={handleSubmit}
              sx={{ mt: 3 }}
            >
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Slide
                    direction="up"
                    in={true}
                    style={{ transitionDelay: "1000ms" }}
                    timeout={800}
                    container={containerRef.current}
                  >
                    <span>
                      <TextField
                        autoComplete="given-name"
                        name="username"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        autoFocus
                        onChange={(event) => handleChangeUsername(event)}
                        value={data.username}
                        error={
                          data.username.length > 0 && !data.isValidUsername
                        }
                        helperText={
                          data.username.length > 0 && !data.isValidUsername
                            ? "Username must be at least 3 characters long and contain only alphanumeric characters"
                            : ""
                        }
                      />
                    </span>
                  </Slide>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    onChange={(event) => handleChangeEmail(event)}
                    value={data.email}
                    error={data.email.length > 0 && !data.isValidEmail}
                    helperText={
                      data.email.length > 0 && !data.isValidEmail
                        ? "Please enter a valid email address"
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="new-password"
                    value={data.password}
                    onChange={(event) => handleChangePassword(event)}
                    error={data.password.length > 0 && !data.isValidPassword}
                    helperText={
                      data.password.length > 0 && !data.isValidPassword
                        ? "Password must be at least 8 characters long"
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password confirmation"
                    label="Password confirmation"
                    type="password"
                    id="passwordConfirmation"
                    autoComplete="new-password"
                    value={data.passwordConfirmation}
                    onChange={(event) =>
                      handleChangePasswordConfirmation(event)
                    }
                    error={
                      data.passwordConfirmation.length > 0 &&
                      !data.isValidPasswordConfirmation
                    }
                    helperText={
                      data.passwordConfirmation.length > 0 &&
                      !data.isValidPasswordConfirmation
                        ? "Passwords does not match"
                        : ""
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        color="primary"
                        checked={data.isValidPrivacy}
                        onChange={handleChangePrivacy}
                      />
                    }
                    label={
                      <>
                        I have read and accept terms of the{" "}
                        <CustomLink href="/privacy">privacy policy.</CustomLink>
                      </>
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        color="primary"
                        checked={data.isValidAge}
                        onChange={handleChangeAge}
                      />
                    }
                    label="I am 13 years or older."
                  />
                </Grid>
              </Grid>
              {alert.type && (
                <Alert severity={alert.type as AlertColor}>
                  {alert.message}
                </Alert>
              )}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
                color="secondary"
                disabled={!isVerified || loading}
              >
                {loading ? "Signing Up..." : "Sign Up"}
              </Button>
              {warning && (
                <Typography variant="caption" color="error">
                  {warning}
                </Typography>
              )}
              <Grid container justifyContent="flex-end">
                <Grid item>
                  <CustomLink href="/signin">
                    Already have an account? Sign in
                  </CustomLink>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Container>
      </Fade>
    </>
  );
};

export default SignUp;
