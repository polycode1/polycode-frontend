import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ThemeProvider } from "@mui/material";
import { theme } from "../modules/theme";
import Layout from "../modules/views/Layout";
import { UserProvider } from "../contexts/user/user.providers";
import { ModulesProvider } from "../contexts/modules/modules.providers";
import { HttpProvider } from "../contexts/http/http.providers";
import { ExerciseProvider } from "../contexts/exercise/exercise.providers";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <HttpProvider>
        <UserProvider>
          <ModulesProvider>
            <ExerciseProvider>
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </ExerciseProvider>
          </ModulesProvider>
        </UserProvider>
      </HttpProvider>
    </ThemeProvider>
  );
}

export default MyApp;
