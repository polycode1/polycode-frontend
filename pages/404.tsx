import { CustomLink } from "../modules/components/CustomLink";
import ErrorLayout from "../modules/views/ErrorLayout";
export default function Custom404() {
  return (
    <ErrorLayout message="404 - Not found">
      <CustomLink href="/" underline={false} color="white">
        Teleport to a safer place 🛸
      </CustomLink>
    </ErrorLayout>
  );
}
