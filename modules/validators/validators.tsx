import validator from "validator";

export const isEmail = (email: string): boolean => {
  return validator.isEmail(email);
};

export const isAlphanumeric = (username: string): boolean => {
  return validator.isAlphanumeric(username);
};
