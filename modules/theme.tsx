import { createTheme } from "@mui/material";
import { typography } from "@mui/system";

export const themeOptions = {
  typography: {
    fontFamily: ["Alef", "sans-serif"].join(","),
  },
  palette: {
    type: "light",
    primary: {
      main: "#96217f",
      dark: "#66185b",
    },
    secondary: {
      main: "#ffb72e",
      dark: "#D39A2E",
    },
    divider: "rgba(0,0,0,0.06)",
    success: {
      main: "#00c4b6",
      dark: "#009b8c",
    },
    info: {
      main: "#00a8f0",
      dark: "#008bbc",
    },
    editor: {
      main: "#251c38",
      dark: "#1e1e1e",
    },
    background: {
      paper: "#008bbc",
      default: "#96217f",
    },
  },
};

export const themeLanguage: any = {
  palette: {
    java: "#f8981d",
    python: "#3572A5",
    javascript: "#F0DB4F",
    rust: "#de935f",
  },
};

export const theme = createTheme(themeOptions);

export const themeCode = createTheme({
  typography: {
    fontFamily: ["Fira Code", "monospace"].join(","),
  },
});

export const themeLogo = createTheme({
  typography: {
    fontFamily: ["Fascinate", "display"].join(","),
  },
});
