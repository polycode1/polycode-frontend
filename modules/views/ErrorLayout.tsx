import { Box, Container, Grid } from "@mui/material";
import React, { FC, ReactNode } from "react";
import Typography from "../components/Typography";

const ErrorLayout: FC<{ message: string; children: ReactNode }> = ({
  message,
  children,
}) => {
  return (
    <Grid
      container
      sx={{
        mt: { xs: 0, md: 10 },
        flexDirection: { xs: "column-reverse", md: "row" },
      }}
    >
      <Grid
        sm={12}
        md={6}
        item
        container
        sx={{
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Box>
          <img src="laptop_warning.png" />
        </Box>
      </Grid>
      <Grid
        sm={12}
        md={6}
        container
        item
        display="flex"
        direction="column"
        justifyContent="center"
        spacing={3}
        px={8}
        mb={5}
        sx={{
          alignItems: { xs: "center", md: "start" },
        }}
      >
        <Typography
          color="white"
          marked="center"
          variant="h2"
          mt="20"
          fontFamily={"Fascinate"}
          sx={{
            mt: 10,
            mb: 5,
          }}
        >
          Error
        </Typography>
        <Typography
          color="white"
          variant="h6"
          maxWidth="sm"
          fontFamily="Fira Code, monospace"
          sx={{ mb: 6, mt: { sx: 2, sm: 4 } }}
        >
          {message}
        </Typography>
        {children}
      </Grid>
    </Grid>
  );
};

export default ErrorLayout;
