import { FC, useLayoutEffect, useState } from "react";
import { useModules } from "../../contexts/modules/modules.providers";
import Practice from "../../pages/practice";
import { faInfo, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Alert, Container, Fade, Grid, Tooltip } from "@mui/material";
import { Box } from "@mui/system";
import { useEffect } from "react";

import Button from "../components/Button";
import { CardModule, getLastExerciseSubmitted } from "../components/CardModule";
import Typography from "../components/Typography";
import { Exercise, Module } from "../../contexts/modules/modules.types";
import { CustomLink } from "../components/CustomLink";
import { EXERCISE_STORE } from "../../contexts/exercise/exercise.store";
import { useExerciseDispatch } from "../../contexts/exercise/exercise.providers";
import { useUser } from "../../contexts/user/user.providers";

type Props = {
  type: "challenge" | "practice";
  userId: number;
};

const getModulesSection = (modulesGlobal: boolean, modules: Module[]) => (
  <>
    {modules && modules.length > 0 && (
      <>
        <Typography
          color="inherit"
          variant="h4"
          fontWeight={600}
          sx={{ mt: 8, mb: 1 }}
        >
          {modulesGlobal
            ? `Discover new ${
                modules[0].type === "challenge"
                  ? modules[0].type + "s"
                  : "exercises"
              }`
            : "Modules you've started"}
        </Typography>
        <Grid
          container
          item
          ml={0}
          xs={12}
          display="flex"
          justifyItems={"flex-start"}
        >
          {modules.map((module, index) => (
            <CardModule key={`${module.id}-${module.name}`} module={module} />
          ))}
        </Grid>
      </>
    )}
  </>
);

export const Modules: FC<Props> = ({ type, userId }) => {
  const dispatch = useExerciseDispatch();

  const { user } = useUser();
  const isVerified = user?.verified ?? false;

  const handleClickImg = (module: Module, exercise: Exercise) => {
    dispatch({
      type: EXERCISE_STORE.SET_EXERCISE,
      payload: {
        module: module,
        currentExercise: exercise,
        finished: getLastExerciseSubmitted(module),
        total: module.exercises.length,
      },
    });
  };

  const showProgressPerExercise = (module: Module) => (
    <ol>
      {module.exercises.map((exercise) => (
        <li
          key={`${exercise.name}-${exercise.id}`}
          onClick={() =>
            module.type === "practice" ? handleClickImg(module, exercise) : ""
          }
        >
          <>
            {/* <button onClick={handleClickImg(module, exercise)}>Go exo</button> */}
            {module.type === "practice" ? (
              <CustomLink
                href={`exercise?id=${exercise.id}`}
                color="white"
                underline={false}
              >
                {`${exercise.name} : ${
                  exercise.submissions?.[0]?.status ?? "no submissions"
                }`}
              </CustomLink>
            ) : (
              <>
                {`${exercise.name} : ${
                  exercise.submissions?.[0]?.status ?? "no submissions"
                }`}
              </>
            )}
          </>
        </li>
      ))}
    </ol>
  );

  const { modules, setLoad } = useModules();

  useEffect(() => {
    setLoad(true);
  }, []);

  const [userModules, setUserModules] = useState<Module[] | null>(null);
  const [trendModule, setTrendModule] = useState<Module | null>(null);
  const [newModules, setNewModules] = useState<Module[] | null>(null);

  useEffect(() => {
    if (!modules || modules.length === 0) {
      return;
    }
    const typeModules: Module[] = modules.filter(
      (module: Module) => module.type === type
    );

    const userModulesConst = typeModules.filter(
      (module) =>
        module.exercises.filter((exo) => exo.submissions.length > 0).length > 0
    );
    setUserModules(userModulesConst);

    //Set the trend module where module name contain journey to Mars
    setTrendModule(
      modules.filter((module) =>
        module.name
          .toLowerCase()
          .includes(
            type === "challenge" ? "journey to mars" : "basis of coding"
          )
      )[0] ?? modules[0]
    );
    setNewModules(
      typeModules.filter(
        (module) =>
          !userModulesConst
            .map((moduleUser) => moduleUser.id)
            .includes(module.id)
      )
    );
  }, [modules]);

  const showModuleInfo = (module: Module) => {
    return (
      <>
        <Typography>{module.name}</Typography>
        <Typography variant="subtitle2">Languages :</Typography>
        <ul>
          {module.languages.map((language) => (
            <li key={`${language.value}`}>{language.value}</li>
          ))}
        </ul>
        <Typography variant="subtitle2">Exercises :</Typography>
        {showProgressPerExercise(module)}
      </>
    );
  };

  return (
    <Container
      sx={{
        paddingX: 3,
        minWidth: "100%",
        color: "white",
      }}
    >
      {!isVerified && (
        <Alert severity="warning" variant="filled">
          You must verified your email address to start coding !
        </Alert>
      )}
      <Grid
        container
        mt={3}
        item
        xs={12}
        sx={{
          backgroundImage: `url("${
            type === "practice" ? "multiple_languages.jpg" : "mars_journey.png"
          }")`,
          backgroundColor: "#96217f", // Average color of the background image.
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          height: "60vh",
          width: "100vw",
          borderRadius: 10,
        }}
      >
        <Grid
          item
          container
          xs={6}
          sx={{
            alignItems: "flex-start",
            flexDirection: "column",
            justifyContent: "flex-end",
            paddingLeft: { xs: 1, sm: 10 },
            paddingBottom: 5,
          }}
        >
          <Typography
            fontFamily={"Fascinate"}
            color="inherit"
            variant="h2"
            marked="center"
            sx={{
              textTransform: "capitalize",
            }}
          >
            {type}
          </Typography>
          <Typography
            color="inherit"
            variant="h6"
            fontFamily="Fira Code, monospace"
            sx={{ mb: 6, mt: { sx: 2, sm: 4 } }}
          >
            {trendModule?.name}
          </Typography>
          <Grid container alignItems="flex-start" spacing={2}>
            <Grid item>
              <Button
                variant="contained"
                size="large"
                color="secondary"
                link="/exercise?id=1"
                sx={{ p: 0 }}
              >
                {trendModule && (
                  <span
                    style={{ padding: "16px 40px" }}
                    onClick={() =>
                      handleClickImg(trendModule, trendModule?.exercises[0])
                    }
                  >
                    Start
                  </span>
                )}
              </Button>
            </Grid>
            <Grid item>
              <Tooltip
                title={trendModule ? showModuleInfo(trendModule) : "Loading..."}
                arrow
                TransitionComponent={Fade}
                TransitionProps={{ timeout: 600 }}
                leaveDelay={200}
                placement={"right"}
              >
                <span>
                  <Button
                    variant="contained"
                    size="large"
                    sx={{
                      backgroundColor: "info.main",
                      "&:hover": {
                        backgroundColor: "info.dark",
                      },
                    }}
                  >
                    <FontAwesomeIcon icon={faInfoCircle} /> Info
                  </Button>
                </span>
              </Tooltip>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      {userModules && getModulesSection(false, userModules)}
      {newModules && getModulesSection(true, newModules)}
    </Container>
  );
};

export default Modules;
