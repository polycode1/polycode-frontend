import { Alert, Slide, Snackbar } from "@mui/material";
import { Box } from "@mui/system";
import { useRouter } from "next/router";
import { SyntheticEvent, useEffect, useState } from "react";
import { useHttp, useHttpDispatch } from "../../contexts/http/http.providers";
import { HTTP_STORE } from "../../contexts/http/http.store";
import { useUser } from "../../contexts/user/user.providers";
import Footer from "../../modules/components/Footer";

import { Header } from "../../modules/components/Header";
import { HorizontalLoader } from "../components/HorizontalLoader";
import { Spinner } from "../components/Spinner";

type Props = {
  children: React.ReactNode;
};
export const Layout: React.FC<Props> = ({ children }) => {
  const { isLoggedIn, loading } = useUser();
  const { status, redirect, message } = useHttp();
  const httpDispatch = useHttpDispatch();

  const router = useRouter();

  useEffect(() => {
    console.log(redirect);
    if (redirect && redirect?.length > 0) {
      router.push(redirect);
      httpDispatch({
        type: HTTP_STORE.CLEAR_HTTP,
      });
    }
  }, [redirect]);

  const [open, setOpen] = useState(true);

  useEffect(() => {
    if (
      status &&
      status.length > 0 &&
      status !== "progressing" &&
      message &&
      message.length > 0
    ) {
      setOpen(true);
    }
  }, [status, message]);
  const handleClose = (_event?: SyntheticEvent | Event, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  function TransitionDown(props: any) {
    return <Slide {...props} direction="down" />;
  }

  return (
    <>
      <Snackbar
        open={open}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        autoHideDuration={3000}
        TransitionComponent={TransitionDown}
        onClose={handleClose}
      >
        <span>
          {status !== "progressing" && message && message.length > 0 && (
            <Alert
              onClose={handleClose}
              severity={status ?? "error"}
              sx={{ width: "100%" }}
            >
              {message}
            </Alert>
          )}
        </span>
      </Snackbar>
      {isLoggedIn && (
        <Slide in={isLoggedIn} timeout={1500}>
          <span>
            <Header />
          </span>
        </Slide>
      )}

      <Box
        sx={{
          margin: 0,
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          minHeight: "100vh",
          paddingLeft: 0,
          flexGrow: 0,
        }}
      >
        <HorizontalLoader loading={status === "progressing"} />
        <Box
          sx={{
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            backgroundColor: "primary.main",
            opacity: 1,
            zIndex: -2,
          }}
        />
        {!loading && children}
        {loading && <Spinner />}
        <Footer />
      </Box>
    </>
  );
};
export default Layout;
