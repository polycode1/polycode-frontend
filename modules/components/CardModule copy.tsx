import { Badge, Box, Grid, LinearProgress } from "@mui/material";
import Link from "next/link";
import React, { FC } from "react";
import Typography from "./Typography";

type Props = {
  children?: React.ReactNode;
  name?: string;
  description?: string;
  progress?: number; //between 0 and 100
  language?: "python" | "java" | "javascript" | "rust";
  link: string;
};

export const CardModule2: FC<Props> = ({
  children,
  name = "Learn react in 90 days",
  description = "Learn react in 90 days",
  progress = 30,
  language = "javascript",
  link = "/exercise/",
}) => {
  let bgImage = "";
  switch (language) {
    case "python":
      bgImage = "python_logo.jpg";
      break;
    case "java":
      bgImage = "java_logo.png";
      break;
    case "javascript":
      bgImage = "javascript_logo.png";
      break;
    case "rust":
      bgImage = "rust_logo.png";
      break;
  }

  return (
    <Grid
      item
      container
      xs={12}
      sm={6}
      md={4}
      lg={3}
      my={1}
      // mx={1}
      // minHeight="40"
      // minWidth="120"
      borderRadius={3}
      sx={{
        // mr: 1,
        backgroundImage: `url("${bgImage}")`,
        backgroundColor: "#96217f", // Average color of the background image.
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        height: "200px",
        width: "auto",
        opacity: 0.7,
        // boxShadow: "0px 0px 0 1px rgba(0,0,0,0)",
        transition: "boxShadow 1s ease-in-out",
        "&:hover": {
          boxShadow: "0px 0px 10px rgba(0,0,0,0.5)",
          cursor: "pointer",
        },
      }}
    >
      <Grid
        item
        xs={12}
        container
        sx={{
          color: "black",
          opacity: 1,
          transition: "opacity 0.3s ease-in-out",
          "&:hover": {
            opacity: 1,
          },
        }}
      >
        <Grid
          item
          xs={12}
          container
          // alignSelf={"flex-start"}
          padding={2}
          height="100%"
        >
          <Typography
            color="inherit"
            variant="h5"
            fontFamily=""
            fontWeight="bold"

            // sx={{ mb: 6, mt: { sx: 2, sm: 4 } }}
          >
            {name}
          </Typography>
          <Grid item xs={12} container sx={{}}>
            <Typography
              color="inherit"
              variant="h6"
              fontFamily="Fira Code, monospace"
              // sx={{ mb: 6, mt: { sx: 2, sm: 4 } }}
            >
              <Link
                href={{
                  pathname: link,
                  query: { id: 1 },
                }}
              >
                {description}
              </Link>
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <LinearProgress
        variant="determinate"
        value={progress}
        sx={{
          height: "10px",
          width: "100%",
          position: "relative",
          bottom: 0,
          alignSelf: "flex-end",
        }}
        color="info"
      />
    </Grid>
  );
};
