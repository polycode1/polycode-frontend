import Typography from "./Typography";
import Link from "next/link";

type Props = {
  children: React.ReactNode;
  color?: string;
  href: string;
  underline?: boolean;
};

export const CustomLink: React.FC<Props> = ({
  children,
  color = "info.main",
  href,
  underline = true,
}) => {
  return (
    <Typography
      sx={{
        textDecoration: underline ? "underline" : "none",
        color: `${color}`,
        cursor: "pointer",
      }}
    >
      <Link href={href}>{children}</Link>
    </Typography>
  );
};
