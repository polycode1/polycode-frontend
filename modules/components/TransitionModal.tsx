import * as React from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "./Button";
import Typography from "./Typography";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "white",
  borderRadius: 10,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  boxShadow: 24,
  p: 4,
};

type Props = {
  children?: React.ReactNode;
  openModal?: boolean;
};

const TransitionModal: React.FC<Props> = ({ children, openModal = true }) => {
  React.useEffect(() => {
    if (openModal) {
      setOpen(openModal);
    }
  }, [openModal]);

  const [open, setOpen] = React.useState(openModal);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      {/* <Button onClick={handleOpen}>Open modal</Button> */}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Typography fontFamily={"Fascinate"} variant="h3" marked="center">
              Congratulations !
            </Typography>
            <Typography sx={{ m: 4 }}>
              You have successfully completed this module !
            </Typography>
            <Button
              onClick={handleClose}
              sx={{
                backgroundColor: "info.main",
                "&:hover": {
                  backgroundColor: "info.dark",
                },
              }}
            >
              Go back to modules list
            </Button>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default TransitionModal;
