import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import Badge from "@mui/material/Badge";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowAltCircleRight,
  faBars,
  faCode,
  faDumbbell,
  faEnvelopesBulk,
  faHamburger,
  faHandFist,
  faRing,
  faUserAstronaut,
  faUserGear,
  faBell,
  faUserNinja,
} from "@fortawesome/free-solid-svg-icons";
import { CustomLink } from "./CustomLink";
import { useUser, useUserDispatch } from "../../contexts/user/user.providers";
import { USER_STORE } from "../../contexts/user/user.store";
import { Avatar, Grid } from "@mui/material";
import { useHttpDispatch } from "../../contexts/http/http.providers";
import { HTTP_STORE } from "../../contexts/http/http.store";
import { height } from "@mui/system";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(3),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
}));

export const Header: React.FC = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [anchorElNotif, setAnchorElNotif] = React.useState<null | HTMLElement>(
    null
  );
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
    React.useState<null | HTMLElement>(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const isNotifMenuOpen = Boolean(anchorElNotif);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleNotifMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNotif(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };
  const handleMenuNotifClose = () => {
    setAnchorElNotif(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMenuNotifClose();
    handleMobileMenuClose();
  };

  const httpDispatch = useHttpDispatch();

  const { user } = useUser();
  const isVerified = user?.verified ?? false;

  const handleLogout = async () => {
    dispatch({
      type: USER_STORE.LOGOUT,
    });
    httpDispatch({
      type: HTTP_STORE.SET_HTTP,
      payload: { status: "success", message: "User logout", redirect: "/" },
    });
    handleMenuClose();
  };

  const handleProfile = () => {
    httpDispatch({
      type: HTTP_STORE.SET_HTTP,
      payload: { status: "success", redirect: "/profile" },
    });
    handleMenuClose();
  };

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleClickModule = (moduleTpe: "practice" | "challenge") => {
    httpDispatch({
      type: HTTP_STORE.SET_HTTP,
      payload: { status: "success", redirect: `/${moduleTpe}` },
    });
    handleMenuClose();
  };

  // const { isLoggedIn } = useUser();
  const dispatch = useUserDispatch();

  const menuNotifId = "menu-notif";
  const renderMenuNotif = (
    <Menu
      anchorEl={anchorElNotif}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={menuNotifId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isNotifMenuOpen}
      onClose={handleMenuNotifClose}
    >
      <MenuItem onClick={handleMenuNotifClose} sx={{ color: "white" }}>
        Verify your email address.
      </MenuItem>
    </Menu>
  );

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleProfile} sx={{ color: "white" }}>
        Profile
      </MenuItem>
      <MenuItem onClick={handleLogout} sx={{ color: "white" }}>
        Logout
      </MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      sx={{
        color: "white",
      }}
      // color='white'
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem
        onClick={() => handleClickModule("challenge")}
        sx={{
          display: "flex",
          justifyContent: "left",
          color: "white",
          height: 50,
        }}
      >
        <FontAwesomeIcon icon={faHandFist} size="2x" width={40} />
        <Typography variant="subtitle2" color="inherit">
          Challenges
        </Typography>
      </MenuItem>
      <MenuItem
        onClick={() => handleClickModule("practice")}
        sx={{
          display: "flex",
          justifyContent: "left",
          color: "white",
          height: 50,
        }}
      >
        <FontAwesomeIcon icon={faDumbbell} fontSize={"1.5rem"} width={40} />
        <Typography variant="subtitle2">Practice</Typography>
      </MenuItem>
      <MenuItem
        onClick={handleNotifMenuOpen}
        sx={{
          display: "flex",
          justifyContent: "left",
          color: "white",
          height: 50,
        }}
      >
        <Box ml={1} mr={0.5}>
          <Badge badgeContent={1} color="error">
            <FontAwesomeIcon icon={faBell} fontSize={"1.5rem"} />
          </Badge>
        </Box>

        <Typography variant="subtitle2" pl={1}>
          Notifications
        </Typography>
      </MenuItem>
      <MenuItem
        onClick={handleProfileMenuOpen}
        sx={{
          display: "flex",
          justifyContent: "left",
          color: "white",
          height: 50,
        }}
      >
        <FontAwesomeIcon icon={faUserNinja} fontSize={"2rem"} width={40} />
        <Typography variant="subtitle2">Profile</Typography>
      </MenuItem>
    </Menu>
  );

  return (
    <Box sx={{ flexGrow: 1, position: "sticky" }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="open drawer"
            sx={{ mr: 2 }}
          >
            <Avatar
              sx={{ width: 72, height: 72, backgroundColor: "secondary.main" }}
            >
              <CustomLink href="/" color="common.white">
                <img
                  src={"polycode_bunny_keyboard.png"}
                  alt="logo"
                  width={70}
                />
              </CustomLink>
            </Avatar>
          </IconButton>
          <Grid
            container
            direction="row"
            justifyContent="flex-start"
            spacing={2}
            item
            sm={3}
            md={5}
            lg={6}
            alignItems="flex-end"
            sx={{ display: { xs: "none", sm: "flex" } }}
            // sx={{display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }} >
          >
            <Grid item sm={12} md={4}>
              <Typography
                variant="h6"
                noWrap
                component="div"
                fontFamily={"Fascinate"}
                sx={{ textOverflow: "" }}
              >
                PolyCode
              </Typography>
            </Grid>

            <Grid
              item
              sm={0}
              md={4}
              sx={{ display: { sm: "none", md: "fixed" } }}
            >
              <CustomLink
                href="/challenges"
                color="common.white"
                underline={false}
              >
                Challenges
              </CustomLink>
            </Grid>
            <Grid
              item
              sm={0}
              md={4}
              sx={{ display: { sm: "none", md: "fixed" } }}
            >
              <CustomLink
                href="/practice"
                color="common.white"
                underline={false}
              >
                Practice
              </CustomLink>
            </Grid>
          </Grid>
          <Search>
            <SearchIconWrapper>
              <FontAwesomeIcon icon={faArrowAltCircleRight} />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
            />
          </Search>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            {!isVerified && (
              <IconButton
                size="large"
                aria-label="show 1 new notifications"
                color="inherit"
                onClick={handleNotifMenuOpen}
              >
                <Badge badgeContent={1} color="secondary">
                  <FontAwesomeIcon icon={faBell} />
                </Badge>
              </IconButton>
            )}
            <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <FontAwesomeIcon icon={faBars} />
            </IconButton>
          </Box>
          <Box sx={{ display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <FontAwesomeIcon icon={faBars} />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
      {renderMenuNotif}
    </Box>
  );
};
