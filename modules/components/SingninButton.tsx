import { FC } from "react";
import Button from "./Button";

const SigninButton: FC = () => {
  return (
    <Button
      variant="contained"
      size="large"
      link="/signin"
      sx={{
        backgroundColor: "info.main",
        "&:hover": {
          backgroundColor: "info.dark",
        },
      }}
      // sx={{ minWidth: 0, ml: 1 }}
    >
      Sign In
    </Button>
  );
};

export default SigninButton;
