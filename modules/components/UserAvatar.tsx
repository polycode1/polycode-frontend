import { Avatar } from "@mui/material";
import { FC } from "react";

type Props = {
  size?: number;
};

const UserAvatar: FC<Props> = ({ size = 40 }) => {
  return (
    <Avatar
      sx={{
        width: 132,
        height: 132,
        m: 1,
        bgcolor: "secondary.main",
      }}
    >
      <img src="profile.png" width={128} />
    </Avatar>
  );
};

export default UserAvatar;
