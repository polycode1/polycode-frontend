import {
  Badge,
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  cardMediaClasses,
  Grid,
  IconButton,
  LinearProgress,
  Theme,
  Tooltip,
} from "@mui/material";
import { useTheme } from "@mui/styles";
import Link from "next/link";
import React, { FC, useState } from "react";
import { useExerciseDispatch } from "../../contexts/exercise/exercise.providers";
import { EXERCISE_STORE } from "../../contexts/exercise/exercise.store";
import { Exercise, Module } from "../../contexts/modules/modules.types";
import { themeLanguage } from "../theme";
import { CustomLink } from "./CustomLink";
import Typography from "./Typography";

export const getLastExerciseSubmitted = (module: Module, scoreStarted = 0) => {
  let numberOfSubmittedExercises = 0;
  module.exercises.forEach((exercise) => {
    if (exercise.submissions.length > 0) {
      numberOfSubmittedExercises +=
        exercise.submissions[0].status === "finished" ? 1 : scoreStarted;
    }
  });
  return numberOfSubmittedExercises;
};

export const CardModule: FC<{ module: Module }> = ({ module }) => {
  let bgImage = "";

  //Calculate the progress of the module : it is the number of exercise submitted divide by the number of exercises
  const progressModule = (module: Module): number => {
    const numberOfExercises = module.exercises.length;
    let numberOfSubmittedExercises = getLastExerciseSubmitted(module, 0.5);
    return Math.round((numberOfSubmittedExercises / numberOfExercises) * 100);
  };

  const progress = progressModule(module);

  if (!module) {
    return <>Loading</>;
  }

  if (module.languages.length === 1) {
    switch (module.languages[0].value) {
      case "python":
        bgImage = "python_logo2.png";
        break;
      case "java":
        bgImage = "java_logo.png";
        break;
      case "javascript":
        bgImage = "javascript_logo.png";
        break;
      case "rust":
        bgImage = "rust_logo.png";
        break;
    }
  } else {
    bgImage = "multiple_languages.jpg";
  }

  const theme: Theme = useTheme();

  const dispatch = useExerciseDispatch();

  const handleClickImg = (module: Module, exercise: Exercise | null) => {
    dispatch({
      type: EXERCISE_STORE.SET_EXERCISE,
      payload: {
        module: module,
        currentExercise:
          exercise ?? module.exercises[getLastExerciseSubmitted(module)],
        finished: getLastExerciseSubmitted(module),
        total: module.exercises.length,
      },
    });
  };

  const showProgressPerExercise = (module: Module) => (
    <ol>
      {module.exercises.map((exercise) => (
        <li
          key={`${exercise.name}-${exercise.id}`}
          onClick={() =>
            module.type === "practice" ? handleClickImg(module, exercise) : ""
          }
        >
          <>
            {/* <button onClick={handleClickImg(module, exercise)}>Go exo</button> */}
            {module.type === "practice" ? (
              <CustomLink
                href={`exercise?id=${exercise.id}`}
                color="white"
                underline={false}
              >
                {`${exercise.name} : ${
                  exercise.submissions?.[0]?.status ?? "no submissions"
                }`}
              </CustomLink>
            ) : (
              <>
                {`${exercise.name} : ${
                  exercise.submissions?.[0]?.status ?? "no submissions"
                }`}
              </>
            )}
          </>
        </li>
      ))}
    </ol>
  );

  return (
    <Card sx={{ display: "flex", margin: 2 }}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          backgroundColor: "common.white",
          height: "180px",
        }}
      >
        <CardContent
          sx={{
            flex: "1 1 auto",
            width: { xs: "11rem", sm: "15rem", md: "18rem" },
            pb: 0,
          }}
        >
          <Typography variant="h5" fontWeight={"600"} noWrap>
            {module.name}
          </Typography>
          <Typography
            variant="subtitle1"
            color="text.secondary"
            component="div"
            overflow={"clip"}
            height={"5.5rem"}
          >
            {module.description}
          </Typography>
        </CardContent>

        <Grid container>
          <Grid
            item
            xs={10}
            sx={{ display: "flex", alignItems: "center", pl: 1, pb: 1 }}
          >
            {module.languages.map((language, index) => (
              <Tooltip key={index} title={language.value} placement="top" arrow>
                <span>
                  <Typography
                    key={index}
                    variant="body2"
                    fontFamily={"Fira Code"}
                    fontWeight={900}
                    px={1}
                    py={0.2}
                    mr={1}
                    sx={{
                      cursor: "pointer",
                      backgroundColor: themeLanguage.palette[language.value],
                      borderRadius: "50%",
                    }}
                  >
                    {language.value[0].toUpperCase()}
                  </Typography>
                </span>
              </Tooltip>
            ))}
          </Grid>
          {progress > 0 && (
            <Grid item xs={2}>
              <Tooltip
                title={showProgressPerExercise(module)}
                placement="top"
                arrow
              >
                <span>
                  <Typography
                    variant="subtitle2"
                    color={progress === 100 ? "success.main" : "info.dark"}
                  >
                    {progress + " %"}
                  </Typography>
                </span>
              </Tooltip>
            </Grid>
          )}
        </Grid>
        {progress > 0 && (
          <LinearProgress
            variant="determinate"
            color={progress === 100 ? "success" : "info"}
            value={progress}
            sx={{ width: "100%" }}
          />
        )}
      </Box>
      <CardActionArea>
        <Link href={`/exercise?id=${module.exercises[1].id}`}>
          <CardMedia
            onClick={() => handleClickImg(module, null)}
            component="img"
            sx={{ height: 180 }}
            image={bgImage}
            alt="Live from space album cover"
          />
        </Link>
      </CardActionArea>
    </Card>
  );
};
