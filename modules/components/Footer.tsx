import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Link from "next/link";
import Container from "@mui/material/Container";
import Typography from "./Typography";
import TextField from "./TextField";
import { ThemeProvider } from "@emotion/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
function Copyright() {
  return (
    <>
      {"© "}
      <Link color="inherit" href="https://mui.com/">
        GridexX
      </Link>
      {" - "}
      {new Date().getFullYear()}
    </>
  );
}

const iconStyle = {
  width: 48,
  height: 48,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "warning.main",
  mr: 1,
  "&:hover": {
    bgcolor: "warning.dark",
  },
};

const LANGUAGES = [
  {
    code: "en-US",
    name: "English",
  },
  {
    code: "fr-FR",
    name: "Français",
  },
];

export default function Footer() {
  return (
    <Typography
      component="footer"
      sx={{
        display: "flex",
        bgcolor: "primary.light",
        color: "secondary.light",
      }}
    >
      <Container sx={{ my: 1, display: "flex" }}>
        <Grid container spacing={2}>
          <Grid item xs={6} sm={4} md={3}>
            <Grid
              container
              direction="column"
              justifyContent="flex-end"
              spacing={2}
              sx={{ height: 120 }}
            >
              <Grid
                container
                direction="row"
                marginLeft={0}
                // justifyContent="center"
                spacing={2}
              >
                <Grid item>
                  <FontAwesomeIcon icon={faEnvelope} size="1x" />
                </Grid>
                <Grid item>
                  <FontAwesomeIcon icon={faEnvelope} size="1x" />
                </Grid>
              </Grid>
              <Grid item>
                <Copyright />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={6} sm={4} md={2}>
            <Typography variant="h6" marked="left" gutterBottom>
              Legal
            </Typography>
            <Box component="ul" sx={{ m: 0, listStyle: "none", p: 0 }}>
              <Box component="li" sx={{ py: 0.5 }}>
                <Link href="/premium-themes/onepirate/terms/">Terms</Link>
              </Box>
              <Box component="li" sx={{ py: 0.5 }}>
                <Link href="/privacy">Privacy</Link>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={6} sm={4} md={7}>
            <Typography variant="h6" marked="left" gutterBottom>
              Language
            </Typography>
            <TextField
              select
              size="medium"
              variant="standard"
              SelectProps={{
                native: true,
              }}
              sx={{ mt: 1, width: 150 }}
            >
              {LANGUAGES.map((language) => (
                <option value={language.code} key={language.code}>
                  {language.name}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid item>
            <Typography variant="caption">
              Made in React/Typescript with : ⌨️, ❤️ and ☕ .
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </Typography>
  );
}
