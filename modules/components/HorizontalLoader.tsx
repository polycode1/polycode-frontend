import { LinearProgress } from "@mui/material";

export const HorizontalLoader: React.FC<{ loading?: boolean }> = ({
  loading = true,
}) => {
  return (
    <>
      {loading && (
        <LinearProgress
          sx={{
            height: "5px",
            width: "100%",
            position: "absolute",
            top: 0,
          }}
          color="info"
        />
      )}
    </>
  );
};
