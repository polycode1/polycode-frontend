import { faKeyboard } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

type Props = {
  loading?: boolean;
  color?: string;
};

export const Spinner: React.FC<Props> = ({
  loading = true,
  color = "white",
}) => {
  return (
    <>
      {loading && (
        <FontAwesomeIcon icon={faKeyboard} color={color} fade size="4x" />
      )}
    </>
  );
};
