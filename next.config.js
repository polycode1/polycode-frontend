/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  async redirects() {
    return [
      {
        source: '/challenge',
        destination: '/challenges',
        permanent: true,
      },
    ]
  },
}

module.exports = nextConfig
